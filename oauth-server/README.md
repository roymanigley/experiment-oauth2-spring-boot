# OAuth2
> Infos from https://howtodoinjava.com/spring-boot2/oauth2-auth-server/


## Oauth2-Flow

![Oauth2-Flow](https://howtodoinjava.com/wp-content/uploads/2019/04/Oauth2-Flow.png)  
1. Get authorization grant from resource owner from URL : http://localhost:8080/oauth/authorize?client_id=clientapp&response_type=code&scope=read_profile_info, It will bring a login page. Provide username and password. For this demo, use “admin” and “1234”.  
     
   After login, you will be redirected to grant access page where you choose to grant access to third party application.

2. It will redirect to a URL like : http://localhost:8081/login?code=EAR76A. Here 'EAR76A' is authorization code for the third party application.

3. Use authorization grant to get the access token. Here we need to make following request. Use the code obtained in first step here.  
    ```bash
    authorization: Basic Y2xpZW50YXBwOjEyMzQ1Ng== → base64('clientapp:123456') @see OAuth2AuthorizationServer L: 31,
    Form data - application/x-www-form-urlencoded:   
    grant_type=authorization_code,
    code=EAR76A,
    redirect_uri=http://localhost:8081/login
    ```  
    CURL  
    ```bash
    curl -X POST --user clientapp:123456 http://localhost:8080/oauth/token 
          -H "content-type: application/x-www-form-urlencoded"
          -d "code=EAR76A&grant_type=authorization_code&redirect_uri=http%3A%2F%2Flocalhost%3A8081%2Flogin&scope=read_user_info"
    ```
4. Token as JSON Response:
    ```JSON
    {  
      "access_token": "59ddb16b-6943-42f5-8e2f-3acb23f8e3c1",
      "token_type": "bearer",
      "refresh_token": "cea0aa8f-f732-44fc-8ba3-5e868d94af64",
      "expires_in": 4815,
      "scope": "read_profile_info"
    }
    ```
  
5. Once we have access token, we can go to resource server to fetch protected user data.  
    ```bash
    curl -X GET http://localhost:8080/api/users/me 
       -H "authorization: Bearer 59ddb16b-6943-42f5-8e2f-3acb23f8e3c1"
    ```
  
6. Once we have access token, we can go to resource server to fetch protected user data.  
    ```JSON
    {
      "password":null,
      "username":"admin",
      "authorities":[{"authority":"ROLE_USER"}],
      "accountNonExpired":true,
      "accountNonLocked":true,
      "credentialsNonExpired":true,
      "enabled":true
    }
    ```  

## POC
> POC for Oauth2-Flow from step 3 to 6
```groovy
import groovy.json.*

println "[+] Start grabbing Token"

LOGIN_URL = "http://localhost:8080/oauth/token"
USER_DATA_URL = "http://localhost:8080/api/user"
CLIENTAPP_CREDENTIALS = "clientapp:123456"

String code = null;

System.in.withReader({ reader ->
    println "[+] Enter the code obtained from:\n[+] /oauth/authorize?client_id=clientapp&response_type=code&scope=read_profile_info"
    print " > "
    code = reader.readLine()

});

String token = grabbingToken(code)
if (token != null)
    accessUserData(token)



def grabbingToken(String code) {

    String token = null;

    LOGIN_URL.toURL().openConnection().with { conn ->

        conn.setRequestProperty("User-Agent", "googlebot")
        conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded")
        conn.setRequestProperty(
                "Authorization", "Basic ${CLIENTAPP_CREDENTIALS.bytes.encodeBase64().toString()}"
        )

        conn.requestMethod = "POST"
        conn.doOutput = true
        conn.outputStream << "grant_type=authorization_code&redirect_uri=http%3A%2F%2Flocalhost%3A8081%2Flogin&code=${code}"

        if (conn.responseCode != 200)
            println "[!] Grabbing Token failed"
        else {
            def json = new JsonSlurper().parseText(conn.content.newReader().text);
            /*
            {
                "access_token":"091747a0-7fa4-419c-a658-afc8ac544e46",
                "token_type":"bearer",
                "refresh_token":"9e03040d-d37a-4ea5-8c50-575a3697e85c",
                "expires_in":119,
                "scope":"read_profile_info"
            }
            */
            token = json.access_token
            println "[+] Token found"
            println "[+] access_token:  " + token
            println "[+] refresh_token: " + json.refresh_token
            println "[+] token_type:    " + json.token_type
            println "[+] expires_in:    " + json.expires_in
            println "[+] scope:         " + json.scope

        }
    }
    return token
}

def accessUserData(String token) {

    println "[+] Start accessing user data from resource server"
    USER_DATA_URL.toURL().openConnection().with { conn ->

        conn.setRequestProperty("authorization", "Bearer ${token}")
        if (conn.responseCode != 200)
            println "[!] accessing user data Token failed"
        else {
            def json = new JsonSlurper().parseText(conn.content.newReader().text);
            /*
            {
                "password":null,
                "username":"admin",
                "authorities":[{"authority":"ROLE_USER"}],
                "accountNonExpired":true,
                "accountNonLocked":true,
                "credentialsNonExpired":true,
                "enabled":true
            }
            */

            println "[+] User found"
            println "[+] username:  " + json.username
            println "[+] authority:  " + json.authorities[0].authority
        }
    }
}
```