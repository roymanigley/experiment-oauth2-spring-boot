package ch.bytercrowd.oauth.repository;

import ch.bytercrowd.oauth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
