package ch.bytercrowd.oauth.service;

import ch.bytercrowd.oauth.model.Role;
import ch.bytercrowd.oauth.model.User;
import ch.bytercrowd.oauth.repository.RoleRepository;
import ch.bytercrowd.oauth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    private PasswordEncoder encoder;

    @Autowired
    public UserDetailServiceImpl(UserRepository repository, RoleRepository roleRepository, @Lazy PasswordEncoder encoder) {
        this.userRepository = repository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        if (userRepository.count() < 1)
            createFirstUser();

        User user = userRepository.findUserByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : user.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }


    public void createFirstUser() {
        System.out.println("Adding default User");
        final Role adminRole = new Role();
        adminRole.setName("ADMIN");

        final User adminUser = new User();
        adminUser.setUsername("admin");
        adminUser.setPassword(encoder.encode("admin"));

        adminUser.addRole(
                roleRepository.save(adminRole)
        );
        userRepository.save(adminUser);
    }
}
