package ch.bytercrowd.oauth.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/user")
public class LoginTestController {

    @RequestMapping(method = RequestMethod.GET)
    public User getUsername() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
